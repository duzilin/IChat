package com.du.ichat.bean;

import com.du.ichat.utils.PinYinUtils;

public class Contact {
    private int avatarId;
    private String name;
    private String letter;
    public Contact() {
    }

    public Contact(int avatarId, String name) {
        this.avatarId = avatarId;
        this.name = name;
        setLetter(PinYinUtils.getFirstSpell(name));
    }

    public int getAvatarId() {
        return avatarId;
    }

    public void setAvatarId(int avatarId) {
        this.avatarId = avatarId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        setLetter(PinYinUtils.getFirstSpell(name));
    }


    public String getLetter() {
        return letter;
    }

    private void setLetter(String letter) {
        if(letter.charAt(0) >='A'){
            if(letter.charAt(0) <='Z'){
                this.letter = letter;
                return;
            }
        }
        this.letter = "#";
    }
}
