package com.du.ichat.bean;

public class TestMessage {
    private boolean isSend;
    private String message;
    private int avatar;

    public TestMessage(boolean isSend, String message, int avatar) {
        this.isSend = isSend;
        this.message = message;
        this.avatar = avatar;
    }

    public boolean isSend() {
        return isSend;
    }

    public void setSend(boolean send) {
        isSend = send;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }
}
