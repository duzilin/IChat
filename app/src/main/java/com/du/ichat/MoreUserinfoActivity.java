package com.du.ichat;

import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.du.ichat.databinding.ActivityMoreUserinfoBinding;
import com.du.ichat.utils.BaseActivity;
import com.du.ichat.utils.SpUtils;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.auth.AuthService;
import com.netease.nimlib.sdk.auth.LoginInfo;
import com.netease.nimlib.sdk.uinfo.UserService;
import com.netease.nimlib.sdk.uinfo.constant.UserInfoFieldEnum;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MoreUserinfoActivity extends BaseActivity {
    private static final String[] GENDERS = new String[]{"男" ,"女" };
    private ActivityMoreUserinfoBinding binding;
    private NimUserInfo userinfo;
    private LoginInfo logininfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this , R.layout.activity_more_userinfo);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        userinfo = SpUtils.getObject(this , "userinfo");
        logininfo = SpUtils.getObject(this , "logininfo");
        initView();
    }

    private void initView() {
        setGenderText(userinfo.getGenderEnum().getValue());
        binding.genderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSelectGenderDialog();
            }
        });
        binding.countryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(CountryListActivity.class);
            }
        });
        binding.introduceLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(IntroduceActivity.class);
            }
        });
        binding.linkedInLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



    }
    private AlertDialog dialog;
    private void showSelectGenderDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("性别");
        builder.setSingleChoiceItems(GENDERS, userinfo.getGenderEnum().getValue()-1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                }, 300);
                updateGender(i);
            }
        });
        dialog = builder.create();
        dialog.show();
    }

    private void updateGender(final int gender) {
        final int tempGender = gender+1;
        Map<UserInfoFieldEnum, Object> fields = new HashMap<>(1);
        fields.put(UserInfoFieldEnum.GENDER, tempGender);
        NIMClient.getService(UserService.class).updateUserInfo(fields).setCallback(new RequestCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                fetchUserInfo();
                setGenderText(tempGender);
            }

            @Override
            public void onFailed(int i) {

            }

            @Override
            public void onException(Throwable throwable) {

            }
        });

    }

    private void fetchUserInfo(){
        List<String> list = new ArrayList<>();
        list.add(logininfo.getAccount());
        NIMClient.getService(UserService.class).fetchUserInfo(list).setCallback(new RequestCallback<List<NimUserInfo>>() {
            @Override
            public void onSuccess(List<NimUserInfo> nimUserInfos) {
                if(nimUserInfos.size() > 0){
                    SpUtils.putObject(MoreUserinfoActivity.this , "userinfo" , nimUserInfos.get(0));
                }
            }

            @Override
            public void onFailed(int i) {

            }

            @Override
            public void onException(Throwable throwable) {

            }
        });
    }

    private void setGenderText(int gender){
        String str = "";
        switch (gender){
            case 1:
                str = "男";
                break;
            case 2:
                str = "女";
                break;
        }
        binding.tvGender.setText(str);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(binding != null){
            userinfo = SpUtils.getObject(this , "userinfo");
            logininfo = SpUtils.getObject(this , "logininfo");
            Map<String, Object> map = userinfo.getExtensionMap();
            if(map != null){
                binding.tvCountry.setText(userinfo.getExtensionMap().get("country").toString());
            }else{
                binding.tvCountry.setText("");
            }
            if(userinfo.getSignature() == null){
                binding.tvIntroduce.setText("未填写");
            }else{
                binding.tvIntroduce.setText(userinfo.getSignature());
            }
        }
    }
}
