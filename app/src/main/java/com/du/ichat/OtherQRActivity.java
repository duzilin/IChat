package com.du.ichat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class OtherQRActivity extends AppCompatActivity {
    private TextView tv;
    private Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_qr);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        String str = getIntent().getStringExtra("scanResult");
        tv = findViewById(R.id.other_qr_tv);
        tv.setText(str);
    }


    public void back(View view) {
        this.finish();
    }
}
