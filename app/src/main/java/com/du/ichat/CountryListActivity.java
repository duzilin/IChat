package com.du.ichat;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.du.ichat.databinding.ActivityCountryListBinding;
import com.du.ichat.utils.BaseActivity;
import com.du.ichat.utils.SpUtils;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.auth.LoginInfo;
import com.netease.nimlib.sdk.uinfo.UserService;
import com.netease.nimlib.sdk.uinfo.constant.UserInfoFieldEnum;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CountryListActivity extends BaseActivity {
    private ActivityCountryListBinding binding;
    private List<String> countryList = new ArrayList<>();
    private CountryAdapter adapter;
    private NimUserInfo userinfo;
    private LoginInfo logininfo;
    private TextView tvCountry;
    private SettingActivity.FullDialog dialog;
    private String country;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this , R.layout.activity_country_list);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        countryList.addAll(Arrays.asList(getString(R.string.countryList).split(",")));
        userinfo = SpUtils.getObject(this , "userinfo");
        logininfo = SpUtils.getObject(this , "logininfo");
        dialog = new SettingActivity.FullDialog();

        Map<String, Object> map = userinfo.getExtensionMap();
        if(map != null){
            country = userinfo.getExtensionMap().get("country").toString();
        }else{
            country = "未选择";
        }

        adapter = new CountryAdapter(countryList);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(adapter);
        adapter.setListener(new CountryItemClickListener() {
            @Override
            public void click(int position) {
                dialog.show(getSupportFragmentManager() , "dialog");
                updateCountry(position);
                setCountryText(countryList.get(position));
            }
        });

        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }

    private void updateCountry(int position) {
        Map<String,String> ex =new HashMap<>();
        ex.put("country" , countryList.get(position));
        Map<UserInfoFieldEnum, Object> fields = new HashMap<>(1);
        fields.put(UserInfoFieldEnum.EXTEND, ex);
        NIMClient.getService(UserService.class).updateUserInfo(fields).setCallback(new RequestCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                fetchUserInfo();
            }

            @Override
            public void onFailed(int i) {
                dialog.dismiss();
            }

            @Override
            public void onException(Throwable throwable) {
                dialog.dismiss();
            }
        });
    }


    class CountryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

        private final static int HEADER = 0;
        private final static int ITEM = 1;
        private CountryItemClickListener listener;


        private List<String> data;

        public CountryAdapter(List<String> data) {
            this.data = data;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view;
            if(viewType ==HEADER){
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_country_list_header , parent , false);
                tvCountry = view.findViewById(R.id.tvCountry);
                tvCountry.setText(country);
            }else{
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_country , parent , false);

            }

            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
            if(getItemViewType(position) == HEADER){
                return;
            }
            ViewHolder holder1 = (ViewHolder) holder;
            holder1.tv.setText(data.get(position-1));
            holder1.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(listener!=null){
                        listener.click(position-1 );
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return data.size()+1;
        }

        @Override
        public int getItemViewType(int position) {
            if(position == 0){
                return HEADER;
            }
            return ITEM;
        }

        class ViewHolder extends RecyclerView.ViewHolder{
            TextView tv;
            View view;
            public ViewHolder(View itemView) {
                super(itemView);
                tv = itemView.findViewById(R.id.item_country_tv);
                view = itemView;
            }
        }


        public void setListener(CountryItemClickListener listener) {
            this.listener = listener;
        }
    }

    public interface CountryItemClickListener{
        void click(int position);
    }

    private void setCountryText(String string){
        if(tvCountry != null){
            tvCountry.setText(string);
            adapter.notifyDataSetChanged();
        }
    }


    private void fetchUserInfo(){
        List<String> list = new ArrayList<>();
        list.add(logininfo.getAccount());
        NIMClient.getService(UserService.class).fetchUserInfo(list).setCallback(new RequestCallback<List<NimUserInfo>>() {
            @Override
            public void onSuccess(List<NimUserInfo> nimUserInfos) {
                dialog.dismiss();
                if(nimUserInfos.size() > 0){
                    SpUtils.putObject(CountryListActivity.this , "userinfo" , nimUserInfos.get(0));
//                    finish();
                }
            }

            @Override
            public void onFailed(int i) {
                dialog.dismiss();
            }

            @Override
            public void onException(Throwable throwable) {
                dialog.dismiss();
            }
        });
    }


}
