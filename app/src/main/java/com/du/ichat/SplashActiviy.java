package com.du.ichat;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import com.du.ichat.databinding.ActivitySplashBinding;
import com.du.ichat.utils.BaseActivity;
import com.du.ichat.utils.SpUtils;
import com.du.ichat.utils.Utils;
import com.netease.nimlib.sdk.auth.LoginInfo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.util.Arrays;


public class SplashActiviy extends BaseActivity {
    private static SplashActiviy instance;
    private ActivitySplashBinding binding;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this , R.layout.activity_splash);
        LoginInfo info = SpUtils.getObject(this , "logininfo");
        instance = this;
        if(info!=null){
            startActivity(MainActivity.class);
            this.finish();
            return;
        }
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA,Manifest.permission.READ_PHONE_STATE , Manifest.permission.RECORD_AUDIO} , 101);

        binding.signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(SignUpActivity.class);
            }
        });

        binding.loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(LoginActivity.class);
            }
        });
    }



    public static void finishSplashActivity(){
        if(!instance.isFinishing()){
            instance.finish();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 101){
            int result = 0;
            for (int i = 0; i<grantResults.length;i++){
                result = result+grantResults[i];
            }
            if(result !=0){
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA,Manifest.permission.READ_PHONE_STATE , Manifest.permission.RECORD_AUDIO} , 101);
                toast("请授予软件必须权限");
            }else{
                Utils.copyDefaultAvatarFile(this);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        AlphaAnimation animation = new AlphaAnimation(0,1);
                        animation.setDuration(1000);
                        binding.loginBtn.setVisibility(View.VISIBLE);
                        binding.signupBtn.setVisibility(View.VISIBLE);
                        binding.loginBtn.startAnimation(animation);
                        binding.signupBtn.startAnimation(animation);
                        animation.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @RequiresApi(api = Build.VERSION_CODES.M)
                            @Override
                            public void onAnimationEnd(Animation animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                    }
                } , 2000);
            }
        }
    }
}
