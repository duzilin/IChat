package com.du.ichat;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.du.ichat.databinding.ActivityIntroduceBinding;
import com.du.ichat.utils.BaseActivity;
import com.du.ichat.utils.SpUtils;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.auth.LoginInfo;
import com.netease.nimlib.sdk.uinfo.UserService;
import com.netease.nimlib.sdk.uinfo.constant.UserInfoFieldEnum;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IntroduceActivity extends BaseActivity {
    private ActivityIntroduceBinding binding;
    private NimUserInfo userinfo;
    private LoginInfo logininfo;
    private ChangeNicknameActivity.FullDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this , R.layout.activity_introduce);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        userinfo = SpUtils.getObject(this , "userinfo");
        logininfo = SpUtils.getObject(this , "logininfo");
        dialog = new ChangeNicknameActivity.FullDialog();
        initView();
    }


    private void initView() {
        String introduce = "";
        if(userinfo.getSignature() != null){
            binding.tvTextsun.setText(""+(30-userinfo.getSignature().length()));
            introduce = userinfo.getSignature();
        }else{
            binding.tvTextsun.setText("30");
        }
        binding.etIntroduce.setText(introduce);
        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        binding.etIntroduce.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                binding.tvTextsun.setText("" + (30 - charSequence.toString().length()));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_changenickname , menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_save){
            String introduce = binding.etIntroduce.getText().toString();
            if(TextUtils.isEmpty(introduce)){
                toast("个性签名不能为空！");
                return true;
            }
            dialog.show(getSupportFragmentManager() , "dialog");
            Map<UserInfoFieldEnum, Object> fields = new HashMap<>(1);
            fields.put(UserInfoFieldEnum.SIGNATURE, introduce);
            NIMClient.getService(UserService.class).updateUserInfo(fields).setCallback(new RequestCallback<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    fetchUserInfo();
                }

                @Override
                public void onFailed(int i) {
                    dialog.dismiss();
                }

                @Override
                public void onException(Throwable throwable) {
                    dialog.dismiss();
                }
            });


        }
        return super.onOptionsItemSelected(item);
    }

    private void fetchUserInfo(){
        List<String> list = new ArrayList<>();
        list.add(logininfo.getAccount());
        NIMClient.getService(UserService.class).fetchUserInfo(list).setCallback(new RequestCallback<List<NimUserInfo>>() {
            @Override
            public void onSuccess(List<NimUserInfo> nimUserInfos) {
                dialog.dismiss();
                if(nimUserInfos.size() > 0){
                    SpUtils.putObject(IntroduceActivity.this , "userinfo" , nimUserInfos.get(0));
                    finish();
                }
            }

            @Override
            public void onFailed(int i) {
                dialog.dismiss();
            }

            @Override
            public void onException(Throwable throwable) {
                dialog.dismiss();
            }
        });
    }

}
