package com.du.ichat.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

import com.du.ichat.MainActivity;
import com.du.ichat.R;
import com.du.ichat.fragment.ProfileFragment;
import com.du.ichat.utils.AvatarUtils;

public class AvatarSelectDialog extends DialogFragment {
    private TextView tvTakePhoto,tvSelectPhoto;
    private AvatarSelectDialogInterface avatarDialogListener;
    private AvatarUtils avatarUtils;
    View mView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView=inflater.inflate(R.layout.layout_dialog,container,false);
        avatarUtils = new AvatarUtils(getActivity());
        tvSelectPhoto = mView.findViewById(R.id.selectPhotoBtn);
        tvTakePhoto = mView.findViewById(R.id.takePhotoBtn);
        tvSelectPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(avatarDialogListener != null){
                    avatarDialogListener.selectPhoto();
                    dismiss();
                }
            }
        });
        tvTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(avatarDialogListener != null){
                    avatarDialogListener.takePhoto();
                    dismiss();
                }
            }
        });

        return mView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        //设置dialog相应属性
        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.BOTTOM;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(params);
        //必须设定的属性，否则无法使dialog铺满屏幕，设置其他颜色会出现黑边
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        slideToUp(mView);
    }


    /**
     * 弹出动画
     * @param view
     */
    public static void slideToUp(View view){
        Animation slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                1.0f, Animation.RELATIVE_TO_SELF, 0.0f);

        slide.setDuration(500);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);
        view.startAnimation(slide);

        slide.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

    public static interface AvatarSelectDialogInterface{
        void selectPhoto();
        void takePhoto();
    }


    public void setAvatarDialogListener(AvatarSelectDialogInterface avatarDialogListener) {
        this.avatarDialogListener = avatarDialogListener;
    }
}
