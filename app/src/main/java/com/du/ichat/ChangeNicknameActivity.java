package com.du.ichat;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.du.ichat.databinding.ActivityChangeNicknameBinding;
import com.du.ichat.utils.BaseActivity;
import com.du.ichat.utils.SpUtils;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.uinfo.UserService;
import com.netease.nimlib.sdk.uinfo.constant.UserInfoFieldEnum;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChangeNicknameActivity extends BaseActivity {
    private NimUserInfo userinfo;
    private ActivityChangeNicknameBinding binding;
    private FullDialog loadingDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this , R.layout.activity_change_nickname);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        userinfo = SpUtils.getObject(this , "userinfo");
        initView();
    }

    private void initView() {
        loadingDialog = new FullDialog();
        binding.etNickname.setText(userinfo.getName());
        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_changenickname , menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_save){
            String nickname = binding.etNickname.getText().toString().trim();
            if(TextUtils.isEmpty(nickname)){
                toast("名字不能为空!");
                return true;
            }
            loadingDialog.show(getSupportFragmentManager() , "dialog");
            Map<UserInfoFieldEnum, Object> fields = new HashMap<>(1);
            fields.put(UserInfoFieldEnum.Name, nickname);
            NIMClient.getService(UserService.class).updateUserInfo(fields).setCallback(new RequestCallback<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    List<String> accounts = new ArrayList<>();
                    accounts.add(userinfo.getAccount());
                    NIMClient.getService(UserService.class).fetchUserInfo(accounts).setCallback(new RequestCallback<List<NimUserInfo>>() {
                        @Override
                        public void onSuccess(List<NimUserInfo> nimUserInfos) {
                            loadingDialog.dismiss();
                            if(nimUserInfos.size() >0 ){
                                SpUtils.putObject(ChangeNicknameActivity.this ,"userinfo" , nimUserInfos.get(0));
                                finish();
                            }
                        }

                        @Override
                        public void onFailed(int i) {
                            loadingDialog.dismiss();
                        }

                        @Override
                        public void onException(Throwable throwable) {
                            loadingDialog.dismiss();
                        }
                    });
                }

                @Override
                public void onFailed(int i) {
                    loadingDialog.dismiss();
                }

                @Override
                public void onException(Throwable throwable) {
                    loadingDialog.dismiss();
                }
            });
        }
        return super.onOptionsItemSelected(item);
    }


    @SuppressLint("ValidFragment")
    public static class FullDialog extends DialogFragment {
        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            this.setCancelable(false);
            View view = inflater.inflate(R.layout.dialog_login_loading , null , false);
            TextView tv = view.findViewById(R.id.dialog_tv);
            tv.setText("正在更新...");
            return view;
        }
    }
}
