package com.du.ichat.utils;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PinYinUtils {
    private static HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();

    public static void init(){
        format.setCaseType(HanyuPinyinCaseType.UPPERCASE);
        format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
    }

    public static String toPinYin(String string){
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < string.length(); i++) {
            char c = string.charAt(i);
            try {
                String[] s =PinyinHelper.toHanyuPinyinStringArray(c , format);
                for (String s1 : s){
                    result.append(s1);
                }
            } catch (BadHanyuPinyinOutputFormatCombination badHanyuPinyinOutputFormatCombination) {
                badHanyuPinyinOutputFormatCombination.printStackTrace();
            }
        }
        return result.toString();
    }

    public static boolean isContainChinese(String str) {
        Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
        Matcher m = p.matcher(str);
        if (m.find()) {
            return true;
        }
        return false;
    }

    public static boolean isChinese(String str) {
        String regEx = "[\u4e00-\u9fa5]";
        Pattern pat = Pattern.compile(regEx);
        Matcher matcher = pat.matcher(str);
        boolean flg = false;
        if (matcher.find())
            flg = true;

        return flg;
    }

    public static String getPinyinString(String string){
        StringBuilder result = new StringBuilder();
        if(PinYinUtils.isContainChinese(string)){
            for (int i = 0; i < string.length(); i++) {
                String item = string.charAt(i) + "";
                if(PinYinUtils.isChinese(item)){
                    try {
                        String[] s = PinyinHelper.toHanyuPinyinStringArray(string.charAt(i) ,format);
                        if(s.length > 0){
                            result.append(s[0]);
                        }
                    } catch (BadHanyuPinyinOutputFormatCombination badHanyuPinyinOutputFormatCombination) {
                        badHanyuPinyinOutputFormatCombination.printStackTrace();
                    }
                }else{
                    result.append(item.toUpperCase());
                }
            }
        }else{
            return string;
        }
        return result.toString();
    }

    /**
     * 获取第一个字的拼音首字母
     * @param chinese
     * @return
     */
    public static String getFirstSpell(String chinese) {
        if(isContainChinese(chinese)){
            String temp = getPinyinString(chinese);
            return temp.toUpperCase().substring(0,1);
        }else{
            return chinese.toUpperCase().substring(0,1);
        }
    }

}
