package com.du.ichat.utils;

import com.netease.nimlib.sdk.msg.MessageBuilder;
import com.netease.nimlib.sdk.msg.constant.SessionTypeEnum;
import com.netease.nimlib.sdk.msg.model.IMMessage;

public class MessageCreater {
    public static IMMessage getP2PTestMessage(String account , String text){
        return MessageBuilder.createTextMessage(account, SessionTypeEnum.P2P, text);
    }
}
