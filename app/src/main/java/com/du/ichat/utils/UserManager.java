package com.du.ichat.utils;

import android.content.Context;

import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.auth.LoginInfo;
import com.netease.nimlib.sdk.uinfo.UserService;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;

import java.util.List;

public class UserManager {

    public static NimUserInfo getUserinfo(Context context){
        NimUserInfo userinfo = SpUtils.getObject(context , "userinfo");
        return userinfo;
    }


    public static LoginInfo getLoginInfo(Context context){
        LoginInfo loginInfo = SpUtils.getObject(context , "logininfo");
        return loginInfo;
    }


    public static List<NimUserInfo> getFriends(Context context){
        List<NimUserInfo> friends = SpUtils.getObject(context , "friends");
        return friends;
    }

    public static NimUserInfo getUserInfoFromDataBase(String account){
        NimUserInfo user = NIMClient.getService(UserService.class).getUserInfo(account);
        return user;
    }

}
