package com.du.ichat.utils;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

public class BaseActivity extends AppCompatActivity {

    private static boolean LogSwitch = true;
    private static String logTag = "tag";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ScreenUtils.setCustomDensity(this , getApplication());
    }

    public void toast(String str){
        Toast.makeText(this , str , Toast.LENGTH_SHORT).show();
    }

    public void log(String str){
        if(!LogSwitch){
            return;
        }
        Log.e(logTag , str);
    }

    public void showLog(boolean logSwitch){
        BaseActivity.LogSwitch = logSwitch;
    }

    public void setLogTag(String logTag){
        BaseActivity.logTag = logTag;
    }

    public void startActivity(Class clazz){
        Intent intent = new Intent(this , clazz);
        startActivity(intent);
    }

    public void startActivityAndFinish(Class clazz){
        Intent intent = new Intent(this , clazz);
        startActivity(intent);
        finish();
    }

    /**
     * 窗口全屏
     */
    public void setFullScreen() {
        this.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    /**
     * [沉浸状态栏]
     */
    public void steepStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // 透明状态栏
            getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // 透明导航栏
            getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
    }



}
