package com.du.ichat.utils;

import android.util.Log;

import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by DuZiLin on 2018/1/10.
 */

public class RetrofitUtil {
    private static Gson gson = new Gson();
    private static String url = "https://219.143.8.170:8443";
    private static HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
        @Override
        public void log(String message) {
            //打印retrofit日志
            Log.e("retrofit tag", "retrofitNetwork --->" + message);
        }
    });

    private static OkHttpClient client = new OkHttpClient.Builder()
//            .addInterceptor(new Interceptor() {
//        @Override
//        public Response intercept(Chain chain) throws IOException {
//            Request oldRequest = chain.request();
//            return chain.proceed(oldRequest);
//        }
//    })
            .addInterceptor(loggingInterceptor)
            .readTimeout(60 , TimeUnit.SECONDS)
            .connectTimeout(10 , TimeUnit.SECONDS)
            .build();

    static {
        /* 可以通过 setLevel 改变日志级别
 共包含四个级别：NONE、BASIC、HEADER、BODY

NONE 不记录

BASIC 请求/响应行
--> POST /greeting HTTP/1.1 (3-byte body)
<-- HTTP/1.1 200 OK (22ms, 6-byte body)

HEADER 请求/响应行 + 头

--> Host: example.com
Content-Type: plain/text
Content-Length: 3

<-- HTTP/1.1 200 OK (22ms)
Content-Type: plain/text
Content-Length: 6

BODY 请求/响应行 + 头 + 体
*/
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        client.interceptors().add(new Interceptor() {
//            @Override
//            public Response intercept(Chain chain) throws IOException {
//                Request request = chain.request();
//                Request.Builder newBuilder = request.newBuilder().addHeader("test" , "test");
//                return chain.proceed(newBuilder.build());
//            }
//        });
    }

    public static Retrofit retrofit = new Retrofit.Builder()
            //增加返回值为String的支持
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addConverterFactory(JacksonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(url)
            .client(client)
            .build();
    public static <T> T create(Class<T> clazz){
        T t = (T) retrofit.create(clazz);

        return t;
    }

    public static <E> io.reactivex.Observable<E> addThreadTyep(io.reactivex.Observable<E> observable){
        return observable.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io());
    }

    public static void setBaseUrl(String url){
        RetrofitUtil.url = url;
        retrofit = new Retrofit.Builder()
                //增加返回值为String的支持
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(url)
                .client(client)
                .build();
    }



}
