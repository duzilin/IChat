package com.du.ichat.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.du.ichat.R;

public class DialogUtils {


    public static DialogFragment getDialog(String string){
        return new FullDialog(string);
    }

    public static DialogFragment getDialog(){
        return getDialog("正在加载...");
    }

    @SuppressLint("ValidFragment")
    public static class FullDialog extends DialogFragment {

        private String text;

        public FullDialog(String text) {
            this.text = text;
        }

        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            this.setCancelable(false);
            View view = inflater.inflate(R.layout.dialog_login_loading , null , false);
            TextView tv = view.findViewById(R.id.dialog_tv);
            tv.setText(text);
            return view;
        }
    }


    public static void dismiss(DialogFragment dialog , Activity activity){
        Log.e("Tag" , "dialog : " + (dialog != null));
        Log.e("Tag" , "dialog : " + !activity.isFinishing());
        if(dialog != null && !activity.isFinishing()){
            dialog.dismiss();
        }
    }

}
