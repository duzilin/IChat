package com.du.ichat.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.du.ichat.GlideApp;
import com.du.ichat.R;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.netease.nimlib.sdk.uinfo.UserService;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class RecentSessionAdapter extends RecyclerView.Adapter<RecentSessionAdapter.RecentHolder> {

    private List<RecentContact> datas;

    private static final SimpleDateFormat sdf = new SimpleDateFormat("mm月dd日");

    @NonNull
    @Override
    public RecentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recent_session , parent , false);
        return new RecentHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecentHolder holder, int position) {
        final RecentContact item = datas.get(position);
        NimUserInfo user = NIMClient.getService(UserService.class).getUserInfo(item.getContactId());
        GlideApp.with(holder.iv).load(user.getAvatar()).error(R.drawable.default_avatar).placeholder(R.drawable.default_avatar).into(holder.iv);
        holder.tvMessage.setText(item.getContent());
        holder.tvNick.setText(item.getFromNick());
        holder.tvTime.setText(getTimeString(item.getTime()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener != null){
                    listener.onRecentItemClick(item.getContactId());
                }
            }
        });

    }

    @Override
    public int getItemCount() {

        return datas == null ? 0 : datas.size();
    }

    public void setDatas(List<RecentContact> datas) {
        Log.e("Tag" ,"rsa : " + datas.size());
        this.datas = datas;
        notifyDataSetChanged();
    }

    class RecentHolder extends RecyclerView.ViewHolder{
        ImageView iv;
        TextView tvNick,tvMessage,tvTime;
        public RecentHolder(View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.item_recent_session_iv);
            tvNick = itemView.findViewById(R.id.item_recent_session_tvnick);
            tvMessage = itemView.findViewById(R.id.item_recent_session_tvmessage);
            tvTime = itemView.findViewById(R.id.item_recent_session_tvtime);
        }
    }


    private String getTimeString(long time){
        Calendar currCalendar = Calendar.getInstance();
        Calendar timeCalendar = Calendar.getInstance();
        timeCalendar.setTimeInMillis(time);
        if(currCalendar.get(Calendar.YEAR) != timeCalendar.get(Calendar.YEAR)){
            return timeCalendar.get(Calendar.YEAR)+"年" + timeCalendar.get(Calendar.MONTH) + "月";
        }
        if(currCalendar.get(Calendar.MONTH) != timeCalendar.get(Calendar.MONTH)){
            return timeCalendar.get(Calendar.MONTH)+"月" + timeCalendar.get(Calendar.DAY_OF_MONTH) + "日";
        }
        if(currCalendar.get(Calendar.DAY_OF_MONTH) != timeCalendar.get(Calendar.DAY_OF_MONTH)){
            if((currCalendar.get(Calendar.DAY_OF_MONTH) - timeCalendar.get(Calendar.DAY_OF_MONTH)) == 1){
                return "昨天 " + timeCalendar.get(Calendar.HOUR_OF_DAY ) + ":" + timeCalendar.get(Calendar.MINUTE );
            }
            return timeCalendar.get(Calendar.DAY_OF_MONTH)+"月" + timeCalendar.get(Calendar.DAY_OF_MONTH) + "日";
        }

        String pre = timeCalendar.get(Calendar.AM_PM)==0?"上午":"下午";
        String min = timeCalendar.get(Calendar.MINUTE ) + "";
        if(min.length() == 1){
            min = "0" + 1;
        }
        return pre + " " + timeCalendar.get(Calendar.HOUR ) + ":" + min;
    }

    public interface OnRecentItemClickListener{
        void onRecentItemClick(String account);
    }

    private OnRecentItemClickListener listener;

    public void setListener(OnRecentItemClickListener listener) {
        this.listener = listener;
    }
}
