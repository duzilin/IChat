package com.du.ichat.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.du.ichat.R;
import com.du.ichat.utils.EmojiStringUtils;
import com.netease.nimlib.sdk.msg.model.IMMessage;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;

import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int SEND_MESSAGE_VIEW_TYPE = 0;
    private static final int RECIVE_MESSAGE_VIEW_TYPE = 1;
    private NimUserInfo friendInfo,myInfo;
    private List<IMMessage> datas = null;

    public MessageAdapter(NimUserInfo friendInfo, NimUserInfo myInfo) {
        this.friendInfo = friendInfo;
        this.myInfo = myInfo;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == SEND_MESSAGE_VIEW_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_send_message, parent, false);
            return new SendMessageViewHolser(view);
        } else if (viewType == RECIVE_MESSAGE_VIEW_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recive_message, parent, false);
            return new ReciveMessageViewHolser(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        IMMessage item = datas.get(position);
        if (viewHolder instanceof SendMessageViewHolser) {
            SendMessageViewHolser holser = (SendMessageViewHolser) viewHolder;
            holser.ivSendAvatar.setImageResource(R.drawable.default_avatar);
            Glide.with(holser.ivSendAvatar).load(myInfo.getAvatar()).into(holser.ivSendAvatar);
            holser.tvSendMessage.setText(EmojiStringUtils.getEmojiString(holser.ivSendAvatar.getContext() , item.getContent()));
        } else if (viewHolder instanceof ReciveMessageViewHolser) {
            ReciveMessageViewHolser holser = (ReciveMessageViewHolser) viewHolder;
            holser.ivReciveAvatar.setImageResource(R.drawable.default_avatar);
            Glide.with(holser.ivReciveAvatar).load(friendInfo.getAvatar()).into(holser.ivReciveAvatar);
            holser.tvReciveMessage.setText(EmojiStringUtils.getEmojiString(holser.ivReciveAvatar.getContext() , item.getContent()));
        }
    }

    @Override
    public int getItemCount() {
        return datas == null ? 0 : datas.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (myInfo.getAccount().equals(datas.get(position).getFromAccount())) {
            return SEND_MESSAGE_VIEW_TYPE;
        }
        return RECIVE_MESSAGE_VIEW_TYPE;
    }

    class ReciveMessageViewHolser extends RecyclerView.ViewHolder {
        ImageView ivReciveAvatar;
        TextView tvReciveMessage;

        public ReciveMessageViewHolser(View itemView) {
            super(itemView);
            ivReciveAvatar = itemView.findViewById(R.id.recive_message_avatar);
            tvReciveMessage = itemView.findViewById(R.id.recive_message_tv);
        }
    }

    class SendMessageViewHolser extends RecyclerView.ViewHolder {
        ImageView ivSendAvatar;
        TextView tvSendMessage;

        public SendMessageViewHolser(View itemView) {
            super(itemView);
            ivSendAvatar = itemView.findViewById(R.id.send_message_avatar);
            tvSendMessage = itemView.findViewById(R.id.send_message_message);
        }
    }

    public void setDatas(List<IMMessage> datas) {
        this.datas = datas;
        notifyDataSetChanged();
    }

    public void addItem(IMMessage item){
        datas.add(item);
        notifyDataSetChanged();
    }

    public void addDatas(List<IMMessage> datas){
        this.datas.addAll(datas);
        notifyDataSetChanged();
    }

}
