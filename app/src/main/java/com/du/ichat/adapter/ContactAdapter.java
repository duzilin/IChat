package com.du.ichat.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.du.ichat.GlideApp;
import com.du.ichat.R;
import com.du.ichat.utils.ContactComparer;
import com.du.ichat.utils.PinYinUtils;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;

import java.util.Collections;
import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter {

    private final static int HEADER = 1;
    private final static int ITEM = 0;

    private OnContactItemClickListener contactItemClickListener;
    private onHeaderItemClickListener headerItemClickListener;
    private List<NimUserInfo> datas;

    public ContactAdapter(List<NimUserInfo> datas) {
        this.datas = datas;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int itemtype) {
        if(itemtype == HEADER){
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.header_contact , viewGroup , false);
            return new HeaderViewHolder(view);
        }
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_contact , viewGroup , false);
        return new ContactItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if(viewHolder instanceof ContactItemViewHolder){
            int truePosition = i-1;
            final NimUserInfo item = datas.get(truePosition);
            ContactItemViewHolder holder = (ContactItemViewHolder) viewHolder;
            int ascii = getFirstSpellAscii(truePosition);
            if(truePosition == getPositionForFirstSpellAscii(ascii)){
                holder.tvTitle.setVisibility(View.VISIBLE);
                holder.tvTitle.setText(getLetter(item));
            }else{
                holder.tvTitle.setVisibility(View.GONE);
            }
//            holder.iv.setImageResource(item.getAvatarId());
//            Glide.with(holder.iv).load(item.getAvatar()).into(holder.iv);
            GlideApp.with(holder.iv).load(item.getAvatar()).placeholder(R.drawable.default_avatar).error(R.drawable.default_avatar).into(holder.iv);
            holder.tvName.setText(item.getName());
            holder.root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(contactItemClickListener != null){
                        contactItemClickListener.click(item.getAccount());
                    }
                }
            });
        }else if(viewHolder instanceof HeaderViewHolder){
            HeaderViewHolder holder = (HeaderViewHolder) viewHolder;
            holder.newFriendLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    headerItemClick(0);
                }
            });
            holder.groupLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    headerItemClick(1);
                }
            });
            holder.officialAccounts.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    headerItemClick(3);
                }
            });
            holder.tagLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    headerItemClick(2);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return datas.size() +1;
    }


    class ContactItemViewHolder extends RecyclerView.ViewHolder{
        ImageView iv;
        TextView tvName,tvTitle;
        View root;
        public ContactItemViewHolder(@NonNull View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.item_contact_iv);
            tvName = itemView.findViewById(R.id.item_contact_tv);
            tvTitle = itemView.findViewById(R.id.item_contact_title);
            root = itemView;
        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder{
        private View newFriendLayout,groupLayout,officialAccounts,tagLayout;
        public HeaderViewHolder(View itemView) {
            super(itemView);
            newFriendLayout = itemView.findViewById(R.id.newFriendLayout);
            groupLayout = itemView.findViewById(R.id.qunliaoLayout);
            officialAccounts = itemView.findViewById(R.id.gongzhonghaoLayout);
            tagLayout = itemView.findViewById(R.id.tagLayout);
        }
    }


    public void setDatas(List<NimUserInfo> datas) {
        Collections.sort(datas , new ContactComparer());
        this.datas = datas;
        notifyDataSetChanged();
    }

    private int getFirstSpellAscii(int position){
        return getLetter(datas.get(position)).charAt(0);
    }

    public int getPositionForFirstSpellAscii(int ascii){
        for (int i = 0 ; i <getItemCount()-1 ; i++){
            String currAscii = getLetter(datas.get(i));
            char firstChar = currAscii.toUpperCase().charAt(0);
            if(firstChar == ascii){
                return i;
            }
        }
        return -1;
    }

    public String getLetter(NimUserInfo userinfo) {
        String name = userinfo.getName();
        String letter = PinYinUtils.getFirstSpell(name);
        if(letter.charAt(0) >='A'){
            if(letter.charAt(0) <='Z'){
                return letter;
            }
        }
        return "#";
    }


    @Override
    public int getItemViewType(int position) {
        if(position == 0){
            return HEADER;
        }
        return ITEM;
    }


    public static interface OnContactItemClickListener{
        void click(String string);
    }

    public void setContactItemClickListener(OnContactItemClickListener contactItemClickListener) {
        this.contactItemClickListener = contactItemClickListener;
    }

    public static interface onHeaderItemClickListener{
        void click(int position);
    }

    public void setHeaderItemClickListener(onHeaderItemClickListener headerItemClickListener) {
        this.headerItemClickListener = headerItemClickListener;
    }

    private void headerItemClick(int position){
        if(headerItemClickListener != null){
            headerItemClickListener.click(position);
        }
    }
}
