package com.du.ichat.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.du.ichat.R;
import com.du.ichat.utils.ScreenUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class EmojiFragment extends Fragment {
    private List<String> emojiNames = null;
    private View root;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_emoji , container , false);
        recyclerView = root.findViewById(R.id.emoji_recyclerview);
        emojiNames = new ArrayList<>();
        emojiNames.addAll(getArguments().getStringArrayList("emojis"));
        Log.e("tag" , emojiNames.get(0));
        adapter = new EmojiAdapter();
        recyclerView.setLayoutManager(new GridLayoutManager(getContext() , 7));
        recyclerView.setAdapter(adapter);
        return root;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    class EmojiAdapter extends RecyclerView.Adapter<EmojiAdapter.EmojiViewHolder>{


        @NonNull
        @Override
        public EmojiViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            ImageView iv = new ImageView(parent.getContext());
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT , ViewGroup.LayoutParams.WRAP_CONTENT);
            params.topMargin = ScreenUtils.dpToPx(parent.getContext() , 10);
            params.bottomMargin = ScreenUtils.dpToPx(parent.getContext() , 10);
            params.gravity = Gravity.CENTER;
            iv.setLayoutParams(params);
            return new EmojiViewHolder(iv);
        }

        @Override
        public void onBindViewHolder(@NonNull EmojiViewHolder holder, final int position) {
            try {
                Log.e("tag" , "emojifragment : " + emojiNames.get(position) + ".png" );
                InputStream is = getActivity().getAssets().open("emoji/" + emojiNames.get(position) + ".png");
                Bitmap emoji = BitmapFactory.decodeStream(is);
                holder.iv.setImageBitmap(emoji);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(emojiItemClickListener != null){
                            emojiItemClickListener.onEmojiClick(emojiNames.get(position));
                        }
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return emojiNames.size();
        }

        class EmojiViewHolder extends RecyclerView.ViewHolder{
            ImageView iv;
            public EmojiViewHolder(View itemView) {
                super(itemView);
                iv = (ImageView) itemView;
            }
        }
    }

    public static interface OnEmojiItemClickListener{
        void onEmojiClick(String emojiName);

    }

    private OnEmojiItemClickListener emojiItemClickListener;

    public void setEmojiItemClickListener(OnEmojiItemClickListener emojiItemClickListener) {
        this.emojiItemClickListener = emojiItemClickListener;
    }
}
