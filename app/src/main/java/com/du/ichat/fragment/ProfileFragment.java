package com.du.ichat.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

import com.bumptech.glide.Glide;
import com.du.ichat.GlideApp;
import com.du.ichat.QRCardActivity;
import com.du.ichat.R;
import com.du.ichat.SettingActivity;
import com.du.ichat.databinding.FragmentProfileBinding;
import com.du.ichat.dialog.AvatarSelectDialog;
import com.du.ichat.utils.SpUtils;
import com.du.ichat.utils.Utils;
import com.google.zxing.Utils.Constant;
import com.google.zxing.activity.CaptureActivity;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.RequestCallback;
import com.netease.nimlib.sdk.auth.LoginInfo;
import com.netease.nimlib.sdk.friend.FriendService;
import com.netease.nimlib.sdk.friend.constant.VerifyType;
import com.netease.nimlib.sdk.friend.model.AddFriendData;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    private FragmentProfileBinding binding;
    private LoginInfo loginInfo;
    private NimUserInfo userinfo;
    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        binding = DataBindingUtil.bind(view);
        loginInfo = SpUtils.getObject(getActivity() , "logininfo");
        userinfo = SpUtils.getObject(getActivity() , "userinfo");
        initView();

        return view;
    }

    private int i = 0;
    private void initView() {
        GlideApp.with(this).load(userinfo.getAvatar()).placeholder(R.drawable.default_avatar).error(R.drawable.default_avatar).into(binding.avatarIv);
        binding.tvNickname.setText(userinfo.getName());
        binding.tvAccid.setText("微信号:" + loginInfo.getAccount());

        binding.photoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        binding.moneyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        binding.collectLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("tag" , "collect click");
                final VerifyType verifyType = VerifyType.DIRECT_ADD; // 发起好友验证请求
                String msg = "好友请求附言";
                NIMClient.getService(FriendService.class).addFriend(new AddFriendData("p123456", verifyType, msg))
                        .setCallback(new RequestCallback<Void>() {

                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.e("Tag" , "success");
                            }

                            @Override
                            public void onFailed(int i) {
                                Log.e("Tag" , "fail : " + i);
                            }

                            @Override
                            public void onException(Throwable throwable) {

                            }
                        });
            }
        });
        binding.imgLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        binding.settingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity() , SettingActivity.class);
                getActivity().startActivity(intent);
            }
        });
        binding.userinfoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity() , SettingActivity.class);
                getActivity().startActivity(intent);
            }
        });
        binding.ivQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity() , QRCardActivity.class);
                getActivity().startActivity(intent);
            }
        });


    }



    /**
     * 弹出动画
     * @param view
     */
    public static void slideToUp(View view){
//        Animation slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
//                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
//                1.0f, Animation.RELATIVE_TO_SELF, 0.0f);
        Animation slide = new TranslateAnimation(0,0,0,-view.getHeight());

        slide.setDuration(2000);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);
        view.startAnimation(slide);

        slide.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        loginInfo = SpUtils.getObject(getActivity() , "logininfo");
        userinfo = SpUtils.getObject(getActivity() , "userinfo");
        if(binding != null){
            GlideApp.with(this).load(userinfo.getAvatar()).placeholder(R.drawable.default_avatar).error(R.drawable.default_avatar).into(binding.avatarIv);
            binding.tvNickname.setText(userinfo.getName());
        }
    }
}
