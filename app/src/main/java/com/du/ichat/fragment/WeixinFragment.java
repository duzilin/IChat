package com.du.ichat.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.du.ichat.ChatActivity;
import com.du.ichat.R;
import com.du.ichat.adapter.ContactAdapter;
import com.du.ichat.adapter.RecentSessionAdapter;
import com.netease.nimlib.sdk.NIMClient;
import com.netease.nimlib.sdk.Observer;
import com.netease.nimlib.sdk.RequestCallbackWrapper;
import com.netease.nimlib.sdk.msg.MsgService;
import com.netease.nimlib.sdk.msg.MsgServiceObserve;
import com.netease.nimlib.sdk.msg.model.RecentContact;
import com.netease.nimlib.sdk.uinfo.UserService;
import com.netease.nimlib.sdk.uinfo.model.NimUserInfo;

import java.util.ArrayList;
import java.util.List;

public class WeixinFragment extends Fragment implements RecentSessionAdapter.OnRecentItemClickListener{

    private RecyclerView recyclerView;
    private List<RecentContact> datas = new ArrayList<>();
    private RecentSessionAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_weixin, container, false);
        recyclerView = root.findViewById(R.id.fragment_weixin_recyclerview);
        adapter = new RecentSessionAdapter();
        adapter.setListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        adapter.setDatas(datas);
        return root;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        NIMClient.getService(MsgService.class).queryRecentContacts()
                .setCallback(new RequestCallbackWrapper<List<RecentContact>>() {
                    @Override
                    public void onResult(int code, List<RecentContact> recents, Throwable e) {
                        datas.addAll(recents);
                        adapter.setDatas(datas);
                    }
                });
        //  注册/注销观察者
        NIMClient.getService(MsgServiceObserve.class)
                .observeRecentContact(new RecentObserver(), true);
    }

    @Override
    public void onRecentItemClick(String account) {
        NimUserInfo user = NIMClient.getService(UserService.class).getUserInfo(account);
        Intent intent = new Intent(getContext() , ChatActivity.class);
        intent.putExtra("friendInfo" , user);
        startActivity(intent);
    }


    class RecentObserver implements Observer<List<RecentContact>> {

        @Override
        public void onEvent(List<RecentContact> recentContacts) {
            NIMClient.getService(MsgService.class).queryRecentContacts()
                    .setCallback(new RequestCallbackWrapper<List<RecentContact>>() {
                        @Override
                        public void onResult(int code, List<RecentContact> recents, Throwable e) {
                            datas.clear();
                            datas.addAll(recents);
                            adapter.setDatas(datas);
                        }
                    });
        }
    }




}
