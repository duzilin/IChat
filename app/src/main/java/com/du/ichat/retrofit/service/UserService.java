package com.du.ichat.retrofit.service;

import com.du.ichat.retrofit.bean.SignupBean;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface UserService {
    @FormUrlEncoded

    @POST("/nimserver/user/create.action")
    Call<SignupBean> signup(@Header("AppKey") String AppKey , @Header("Nonce") String Nonce , @Header("CurTime") String CurTime , @Header("CheckSum") String CheckSum , @Header("Content-Type") String ContentType , @Field("accid") String username , @Field("token") String password , @Field("name") String nickname);
}
