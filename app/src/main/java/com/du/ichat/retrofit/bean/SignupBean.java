package com.du.ichat.retrofit.bean;

public class SignupBean {

    /**
     * code : 200
     * info : {"token":"670b14728ad9902aecba32e22fa4f6bd","accid":"000000","name":"000000"}
     */

    private int code;
    private InfoBean info;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public InfoBean getInfo() {
        return info;
    }

    public void setInfo(InfoBean info) {
        this.info = info;
    }

    public static class InfoBean {
        /**
         * token : 670b14728ad9902aecba32e22fa4f6bd
         * accid : 000000
         * name : 000000
         */

        private String token;
        private String accid;
        private String name;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getAccid() {
            return accid;
        }

        public void setAccid(String accid) {
            this.accid = accid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
